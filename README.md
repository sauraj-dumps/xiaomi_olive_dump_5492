## olive-user 10 QKQ1.191014.001 V12.5.4.0.QCNCNXM release-keys
- Manufacturer: xiaomi
- Platform: msm8937
- Codename: olive
- Brand: Xiaomi
- Flavor: olive-user
- Release Version: 10
- Id: QKQ1.191014.001
- Incremental: V12.5.4.0.QCNCNXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: zh-CN
- Screen Density: 320
- Fingerprint: Xiaomi/olive/olive:10/QKQ1.191014.001/V12.5.4.0.QCNCNXM:user/release-keys
- OTA version: 
- Branch: olive-user-10-QKQ1.191014.001-V12.5.4.0.QCNCNXM-release-keys
- Repo: xiaomi_olive_dump_5492


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
